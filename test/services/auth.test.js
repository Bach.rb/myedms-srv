/* eslint-disable global-require */
/* eslint-disable no-undef */
const sinon = require('sinon');
const { expect } = require('chai');
const jwt = require('jsonwebtoken');

describe('Auth Service', () => {
  describe('Get Token', () => {
    it('returns a valid token', () => {
      const validToken = 'eyJhbGciOiJ';
      sinon.stub(jwt, 'sign').returns(validToken);
      const AuthService = require('../../src/services/auth.js')(jwt);
      expect(
        AuthService.getToken({ id: 1, firstName: 'Toto', lastName: 'Titi' })
      ).to.equal(validToken);
    });
  });

  describe('Check Token', () => {
    it('check a valid token', () => {
      const decodedToken = {
        id: 7,
        firstName: 'David',
        lastName: 'London',
        iat: 1589715956,
        exp: 1589802356
      };
      sinon.stub(jwt, 'verify').returns(decodedToken);
      const AuthService = require('../../src/services/auth.js')(jwt);
      expect(AuthService.checkToken('myToken')).to.equal(decodedToken);
    });
  });

  describe('Get Jwt Payload', () => {
    it('gets the correct payload for a user', () => {
      const AuthService = require('../../src/services/auth.js')(jwt);
      const user = { id: 1, firstName: 'Bachir', lastName: 'Rb', age: 26 };
      expect(AuthService.getJwtPayload(user)).to.eql({
        id: 1,
        firstName: 'Bachir',
        lastName: 'Rb'
      });
    });
  });

  describe('Get Jwt Options', () => {
    it('gets the correct options value', () => {
      const AuthService = require('../../src/services/auth.js')(jwt);
      const jwtOptions = AuthService.getJwtOptions();
      expect(jwtOptions).to.be.an('object');
      expect(jwtOptions.expiresIn).to.satisfy((expiresIn) => {
        return typeof expiresIn === 'string' || typeof expiresIn === 'number';
      });
    });
  });
});

afterEach(() => {
  sinon.restore();
});
