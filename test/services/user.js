/* eslint-disable no-undef */
const { expect } = require('chai');
const SequelizeMock = require('sequelize-mock');

const dbMock = new SequelizeMock();
const users = [
  {
    email: 'jsmith@gmail.com',
    password: 'xYu7!.g',
    firstName: 'John',
    lastName: 'Smith'
  },
  {
    email: 'jdoe@gmail.com',
    password: 'xYu7!.g',
    firstName: 'Jane',
    lastName: 'Doe'
  },
  {
    email: 'pgraham@gmail.com',
    password: 'xYu7!.g',
    firstName: 'Paul',
    lastName: 'Graham'
  }
];

const UserModel = dbMock.define(
  'user',
  {
    email: 'jsmith@gmail.com',
    password: 'xYu7!.g',
    firstName: 'John',
    lastName: 'Smith'
  },
  {
    options: {
      autoQueryFallback: false
    }
  }
);

const UserService = require('../../src/services/user.js')(dbMock);

describe('User Service', () => {
  describe('Retrieve all users', () => {
    it('returns the exact number of users', () => {
      UserModel.$queryInterface.$clearResults();
      UserModel.$queueResult(users.map((x) => UserModel.build(x)));
      UserService.getAll().then((result) => {
        expect(result.length).to.equal(3);
      });
    });
  });
});
