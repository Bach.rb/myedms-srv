const express = require('express');

const router = express.Router();

module.exports = (UserService) => {
  // Recupérer un utilisateur par id
  router.get('/:id', (req, res) => {
    UserService.getById(req.params.id).then((user) => {
      res.send(JSON.stringify(user));
    });
  });

  // Recupérer tous les utilisateur
  router.get('/', (req, res) => {
    UserService.getAll().then((users) => {
      res.send(JSON.stringify(users));
    });
  });

  // Ajout d'un utilisateur
  router.post('/', (req, res) => {
    const user = JSON.parse(req.body.user);
    UserService.create(user)
      .then((userCreated) => {
        res.send(JSON.stringify(userCreated));
      })
      .catch((err) => res.send(`Errow while creating user : ${err}`));
  });

  // Update d'un utilisateur
  router.put('/:id', (req, res) => {
    const user = JSON.parse(req.body.user);
    UserService.update(user, req.params.id)
      .then((returnValue) => {
        if (returnValue[0] > 0) res.send(`${returnValue[0]} rows affected`);
        else res.send('No rows affected');
      })
      .catch((err) => res.send(`Errow while updating user : ${err}`));
  });

  // Supprimer un utilisateur
  router.delete('/:id', (req, res) => {
    UserService.deleteById(req.params.id).then(() => {
      res.send(`User n°${req.params.id} deleted`);
    });
  });

  return router;
};
