/* eslint-disable global-require */
const express = require('express');

const router = express.Router();

module.exports = (UserService, AuthService) => {
  router.use((req, res, next) => {
    const token = req.cookies.sessid;
    if (AuthService.checkToken(token)) next();
    else next('Forbidden Access');
  });

  router.use('/users', require('./user.js')(UserService));

  return router;
};
