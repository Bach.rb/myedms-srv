const bcrypt = require('bcrypt');

const saltRounds = 10;

module.exports = {
  hashPassword: (passwd) => {
    return bcrypt.hash(passwd, saltRounds);
  },
  checkPassword: (passwd, hash) => {
    return bcrypt.compare(passwd, hash);
  }
};
