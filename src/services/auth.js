module.exports = (jwt) => {
  return {
    getToken: (user) => {
      const payload = module.exports(jwt).getJwtPayload(user);
      return jwt.sign(
        payload,
        process.env.JWT_KEY,
        module.exports(jwt).getJwtOptions()
      );
    },
    checkToken: (token) => {
      return jwt.verify(token, process.env.JWT_KEY);
    },
    getJwtPayload: (user) => {
      return {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName
      };
    },
    getJwtOptions: () => {
      return {
        expiresIn: `${process.env.JWT_EXPIRATION}`
      };
    }
  };
};
