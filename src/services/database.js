const Sequelize = require('sequelize');

module.exports = {
  init: () => {
    return new Sequelize(
      process.env.DB_NAME,
      process.env.DB_USER,
      process.env.DB_PWD,
      {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: 'postgres'
      }
    );
  },
  sync: (db) => {
    db.sync({ force: true }).then(() => {
        console.log('Database synced successfully!'); // eslint-disable-line
    });
  }
};
