module.exports = (db) => {
  return {
    getAll: () => {
      return db.models.user.findAll();
    },
    getById: (id) => {
      return db.models.user.findOne({ where: { id } });
    },
    getByEmail: (email) => {
      return db.models.user.findOne({ where: { email } });
    },
    create: (user) => {
      return db.models.user.create(user);
    },
    update: (user, id) => {
      return db.models.user.update(user, { where: { id } });
    },

    deleteById: (id) => {
      return db.models.user.destroy({ where: { id } });
    }
  };
};
