const fs = require('fs');
const path = require('path');

const basename = path.basename(module.filename);

module.exports = (db) => {
  fs.readdirSync(__dirname)
    .filter(
      (file) =>
        file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'
    )
    .forEach((file) => {
      db.import(path.join(__dirname, file));
    });
};
