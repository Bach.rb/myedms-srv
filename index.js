const express = require('express');
const bodyparser = require('body-parser');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
const db = require('./src/services/database.js').init();
require('./src/models')(db);

const port = process.env.PORT || 4000;

// Services
const UserService = require('./src/services/user.js')(db);
const AuthService = require('./src/services/auth')(jwt);
const PasswordService = require('./src/services/password');

const app = express();
// Middlewares
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.use(cookieParser());

// Routes
app.use('/api', require('./src/api')(UserService, AuthService));

app.post('/login', (req, res, next) => {
  const email = req.body.login;
  const passwd = req.body.password;
  if (email && passwd) {
    UserService.getByEmail(email).then((user) => {
      PasswordService.checkPassword(passwd, user.password).then((isSame) => {
        if (isSame) {
          res.clearCookie('sessid');
          res.cookie('sessid', AuthService.getToken(user));
          res.send('You have succesfully logged in');
        } else res.send('Wrong credentials');
        next();
      });
    });
  } else next('No Login or password submitted');
});

app.post('/subscribe', (req, res, next) => {
  const email = req.body.login;
  const passwd = req.body.password;
  if (email && passwd) {
    PasswordService.hashPassword(passwd).then((hash) => {
      UserService.create({ email, password: hash })
        .then(() => {
          res.send('You have succesfully subscribed');
          next();
        })
        .catch((err) => next(err));
    });
  } else {
    next('No Login or password submitted');
  }
});

app.get('/logout', (req, res, next) => {
  res.clearCookie('sessid');
  res.send('You have succesfully logged out');
  next();
});

// Middlewares
app.use((err, req, res, next) => {
  res.status(500).send(`Something broke : ${err}`);
  next();
});

app.listen(port, () => {
  console.log(`running at port ${port}`); // eslint-disable-line
});
